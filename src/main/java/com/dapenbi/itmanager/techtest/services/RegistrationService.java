package com.dapenbi.itmanager.techtest.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.dapenbi.itmanager.techtest.models.RegistrationResponse;
import com.dapenbi.itmanager.techtest.models.SmsResponse;
import com.github.javafaker.Faker;


@Service
public class RegistrationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationService.class);
	RegistrationResponse resp = new RegistrationResponse();
	SmsResponse sms = new SmsResponse();
	String[] HEADERS = { 
		"NIP", "NAME", "DATE OF BIRtH", 
		"GENDER", "ADDRESS", "ID NUMBER", "PHONE NUMBER"
	};
	
	public RegistrationResponse generateRandom() throws IOException {

		Faker faker = new Faker();

		String fullname = faker.name().fullName(); // Miss .... / Mr....
		Date birthday = faker.date().birthday();
		String address = faker.address().fullAddress();
		String idNumber = faker.idNumber().ssnValid();
		String phoneNumber = faker.phoneNumber().phoneNumber();

	    LocalDateTime myDateObj = LocalDateTime.now();
	    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	    String NIP = myDateObj.format(myFormatObj);

	    String gendname = fullname.substring(0,4);
		String gender = "M"; 
		if(gendname.contentEquals("Miss")) {
			gender = "F";
		}
		
		resp.setFullname(fullname);
		resp.setBirthday(birthday);
		resp.setGender(gender);
		resp.setAddress(address);
		resp.setIdNumber(idNumber);
		resp.setPhoneNumber(phoneNumber);
		resp.setNip(NIP);
				
	    File file = new File("tmp/sample.csv");
	    BufferedWriter writer;
	    CSVPrinter csvPrinter = null;
	    

	    if (!file.exists()) {
	        try {
				writer = Files.newBufferedWriter(Paths.get("tmp/sample.csv"));
		        csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(
		        		HEADERS
		    		));
			} catch (IOException e) {
				e.printStackTrace();
			}
	    } else {
	        try {
				writer = Files.newBufferedWriter(Paths.get("tmp/sample.csv"), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		        csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
			} catch (IOException e) {
				e.printStackTrace();
			}	        
	    }

	    csvPrinter.printRecord( 
	    	resp.getNip(), resp.getFullname(), resp.getBirthday(),  
	    	resp.getGender(), resp.getAddress(), resp.getIdNumber(),
	    	resp.getPhoneNumber()
	    );
	    csvPrinter.flush();	    
	    
		LOGGER.info("[{}] - csvPrinter created", resp);
	    	    
		return resp; 
	}

	
	public SmsResponse readRandom() throws IOException {
	    Reader in = new FileReader("tmp/sample.csv");

	    Iterable<CSVRecord> records = CSVFormat.DEFAULT
	    	      .withHeader(HEADERS)
	    	      .withFirstRecordAsHeader()
	    	      .parse(in);
	    	    for (CSVRecord record : records) {
	    	    	if(resp.getNip().equals(record.get("NIP"))) {
		    	    	String text = "Name : " + record.get("NAME") + '\n';
	    	    		   text += "Birthday : " + record.get("DATE OF BIRtH") + '\n';
	    	    		   text += "Gender : " + record.get("GENDER") + '\n';
	    	    		   text += "Address : " + record.get("ADDRESS") + '\n';
	    	    		   text += "ID Number : " + record.get("ID NUMBER") + '\n';
	    	    		   text += "Phone Number : " + record.get("PHONE NUMBER") + '\n';
	    	    		   text += "NIP : " + record.get("NIP") + '\n';

		    	    	String path = "/tmp/";
		    	    	String filename = record.get("NIP") + ".txt";
		    	    	try {
			    	    	File file = new File(path+filename);	    	    	
			    	    	FileWriter fw = new FileWriter(file.getAbsoluteFile());
				            BufferedWriter bw = new BufferedWriter(fw);
				            bw.write(text);
				            bw.close();
				            sms.setMessage(record.get("GENDER"));
				            sms.setMsisdn(record.get("PHONE NUMBER"));
		    	    	} catch(IOException e) {
		    				e.printStackTrace();	    	    		
		    	    	}	    	    		
	    	    	}
	    	    }	    
	    
		return sms; 
	}
	
	
}
