package com.dapenbi.itmanager.techtest.models;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class RegistrationResponse { 

	  @SerializedName("fullname")
	  private String fullname;
	  @SerializedName("birthday")
	  private Date birthday;
	  @SerializedName("address")
	  private String address;
	  @SerializedName("idNumber")
	  private String idNumber;
	  @SerializedName("phoneNumber")
	  private String phoneNumber;
	  @SerializedName("nip")
	  private String nip;
	  @SerializedName("gender")
	  private String gender;
	  
	  
	  public void setFullname(String fullname) {
		  this.fullname = fullname; 
	  }
	  public void setBirthday(Date birthday) {
		  this.birthday = birthday; 
	  }
	  public void setGender(String gender) {
		  this.gender = gender; 
	  }
	  public void setAddress(String address) {
		  this.address = address; 
	  }
	  public void setIdNumber(String idNumber) {
		  this.idNumber = idNumber; 
	  }
	  public void setPhoneNumber(String phoneNumber) {
		  this.phoneNumber = phoneNumber; 
	  }
	  public void setNip(String nip) {
		  this.nip = nip; 
	  }
	

	  
	  
	  public String getFullname() {
		  return this.fullname; 
	  }
	  public Date getBirthday() {
		  return this.birthday; 
	  }
	  public String getGender() {
		  return this.gender; 
	  }
	  public String getAddress() {
		  return this.address; 
	  }
	  public String getIdNumber() {
		  return this.idNumber; 
	  }
	  public String getPhoneNumber() {
		  return this.phoneNumber; 
	  }
	  public String getNip() {
		  return this.nip; 
	  }
	  
}
