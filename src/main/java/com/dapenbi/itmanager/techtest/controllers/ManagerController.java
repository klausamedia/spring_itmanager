package com.dapenbi.itmanager.techtest.controllers;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dapenbi.itmanager.techtest.models.RegistrationResponse;
import com.dapenbi.itmanager.techtest.models.SmsResponse;
import com.dapenbi.itmanager.techtest.services.RegistrationService;



@RestController
@RequestMapping(value = "/api/1")
@Validated

public class ManagerController {

	@Autowired
	private RegistrationService regService; 
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ManagerController.class);
	
	public ManagerController() {
		
	}
	
	@GetMapping("/handshake")
	public SmsResponse handshake() throws IOException {

		RegistrationResponse resp = regService.generateRandom();
		
		SmsResponse smsResp = null; 
		
		if(resp != null) {
		
			smsResp = regService.readRandom();			
			
			if(smsResp.getMessage() != null) {
				LOGGER.info("[{}] - Sending SMS ... ", smsResp.getMessage());				
			}
		}
		
		return smsResp; 	
	};

	
	
}
