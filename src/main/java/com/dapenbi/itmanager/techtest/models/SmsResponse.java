package com.dapenbi.itmanager.techtest.models;

import com.google.gson.annotations.SerializedName;

public class SmsResponse {

	  @SerializedName("message")
	  private String message;
	  @SerializedName("msisdn")
	  private String msisdn;
	  
	  public void setMessage(String gender) {
		  if(gender.equals("F")) {
			  this.message = "Hi Miss, your registration is success"; 			  
		  } else {
			  this.message = "Hi Mr, your registration is success"; 			  			  
		  }
	  }

	  public void setMsisdn(String msisdn) {
		  this.msisdn = msisdn; 
	  }
	  
	  public String getMessage() {
		  return this.message;
	  }

	  public String getMsisdn() {
		  return this.msisdn;
	  }
	  
}
